package com.mercaderweb.app.repositories;

import com.mercaderweb.app.model.Gadget;
import com.mercaderweb.app.model.User;
import com.mercaderweb.app.repositories.crud.OrderCrudRepository;
import com.mercaderweb.app.repositories.crud.UserCrudRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Autowired
    private OrderCrudRepository crudRepository;

    @Test
    @Order(1)
    public void testInitialQuery() {
        crudRepository.deleteAll();
        Assertions.assertEquals(0, orderRepository.getAll().size());
    }

    @Test
    @Order(2)
    public void testSaveQuery() {
        Date registerDate = new Date(2000/12/12);
        Date birthDate = new Date(2000/12/12);
        User user1 = new User(1, "nombre1@correo.com", "name1", birthDate, "password1", "password1", "password1", "nombre1@correo.com", "password1", "password1", "password1");
        Map<Integer, Gadget> products = new HashMap<Integer, Gadget>();
        Gadget gadget1 = new Gadget(1, "Brand", "Category", "Name", "Description", 150000, true, 50, "Photograph");
        products.put(1,
                gadget1);
        Map<Integer, Integer> quantities = new HashMap<Integer, Integer>();
        quantities.put(1, 1);
        com.mercaderweb.app.model.Order order1 = new com.mercaderweb.app.model.Order(1, registerDate, "Aprobrada", user1, products, quantities);
        Optional<com.mercaderweb.app.model.Order> order = Optional.ofNullable(orderRepository.create(order1));
        Assertions.assertTrue(order.isPresent());
    }

    @Test
    @Order(3)
    public void testGetById() {
        Optional<com.mercaderweb.app.model.Order> order = orderRepository.getOrder(1);
        Assertions.assertTrue(order.isPresent());
    }

    @Test
    @Order(4)
    public void testGetByStatus() {
        List<com.mercaderweb.app.model.Order> orders = orderRepository.getByStatusAndSalesman("Aprobrada", 1);
        Assertions.assertEquals(1, orders.size());
    }

    @Test
    @Order(5)
    public void testDeleteQuery() {
        crudRepository.deleteAll();
        Assertions.assertEquals(0, orderRepository.getAll().size());
    }

}