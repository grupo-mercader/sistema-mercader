package com.mercaderweb.app.repositories;

import com.mercaderweb.app.model.Gadget;
import com.mercaderweb.app.repositories.crud.GadgetCrudRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GadgetRepositoryTest {

    @Autowired
    private GadgetRepository gadgetRepository;

    @Autowired
    private GadgetCrudRepository crudRepository;

    @Test
    @Order(1)
    void testInitialQuery() {
        crudRepository.deleteAll();

        java.util.List<com.mercaderweb.app.model.Gadget> gadgets = gadgetRepository.getAll();
        org.junit.jupiter.api.Assertions.assertEquals(0, gadgets.size());
    }

    @Test
    @Order(2)
    public void testSaveQuery(){
        Gadget gadget1 = new Gadget(1, "Brand", "Category", "Name", "Description", 150000, true, 50, "Photograph");
        Optional<Gadget> gadget = Optional.ofNullable(gadgetRepository.create(gadget1));
        Assertions.assertTrue(gadget.isPresent());
    }

    @Test
    @Order(3)
    public void testGetById(){
        Optional<Gadget> gadget = gadgetRepository.getById(1);
        Assertions.assertTrue(gadget.isPresent());
    }

    @Test
    @Order(4)
    public void testGetByDescription(){
        List<Gadget> gadgets = gadgetRepository.getByDescription("Description");
        org.junit.jupiter.api.Assertions.assertEquals(1, gadgets.size());
    }

    @Test
    @Order(5)
    public void testDeleteQuery() {
        crudRepository.deleteAll();
        Assertions.assertEquals(0, gadgetRepository.getAll().size());
    }
}